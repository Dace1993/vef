<!DOCTYPE html>
<html lang="lv">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/app/assets/owlcarousel/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/app/assets/owlcarousel/owl.theme.default.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
        <link href="<?php echo get_template_directory_uri(); ?>/app/assets/css/style.css" rel="stylesheet" />
        <title><?php wp_title( '|', true, 'right' ); ?></title>
    </head>

    <body <?php body_class(); ?>>
        <header id="main-header">
            <nav class="navbar">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-9 d-flex align-items-center">
                            <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/app/assets/img/jaunais-logo.png" alt="">
                            </a>

                            <div class="nav-wrap">
                                    <?php  wp_nav_menu( array(
                                        'menu'              => 'Top menu',
                                        'theme_location'    => 'top_navigation',
                                        'depth'             => 3,
                                        'container'         => true,
                                        'container_class'   => 'nav-item btn-li',
                                        'container_id'      => '',
                                        'menu_class'        => 'navbar-nav nav d-xl-flex flex-row',
                                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                        'walker'            => new wp_bootstrap_navwalker())

                                    ); ?>

                                <a href="#" class="close-menu"></a>
                            </div>

                            <a href="#" class="menu-toggle d-xl-none"></a>
                        </div>

                        <div class="col-xl-3 d-none d-xl-flex align-items-center">
                            <div class="second-nav d-flex align-items-center">
                                <?php $args = array(
                                    'post_type' => 'page',
                                    'meta_key' => '_wp_page_template',
                                    'meta_value' => 'views/contact-us.php'
                                );
                                $my_querys = new WP_Query($args)?>
                                <?php if( $my_querys->have_posts()) : ?>
                                    <ul class="nav d-flex flex-row"> 
                                        <?php while ( $my_querys->have_posts()) : $my_querys->the_post(); ?>
                                            <li class="nav-item">
                                                <a href="<?php the_permalink();?>" class="nav-link d-flex align-items-center"><?php the_title();?></a>
                                            </li>
                                        <?php endwhile;?>
                                    </ul>
                                <?php endif;?>

                                <div class="lang-bar dropdown">
                                    <a class="dropdown-toggle d-flex align-items-center" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?php echo pll_current_language('flag'); echo pll_current_language('slug');?></a>

                                    <div class="dropdown-menu">
                                        <?php pll_the_languages(array('show_flags'=>1,'display_names_as' => 'slug')); ?>
                                    </div>
                                </div>

                                <div class="search-form">
                                    <?php get_search_form(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <?php wp_head(); ?>
        </header>
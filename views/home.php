<?php 
/*  
    Template Name: Home
*/
?>

<?php get_header(); 
wp_reset_query();?>

<main id="main-content"> 
    <section class="hero-video d-flex align-items-center justify-content-center">
        <figure>  
            <video autoplay="" loop="" muted="" playsinline="" class="animated-short" poster="<?php the_field('background_image');?>">
				<style>
					@media screen and (max-width: 767px) {
					  body.home {
						background: url("<?php the_field('background_image');?>") 0 0 no-repeat;
						background-size: cover;
					  }
					}
				</style>
                <source src="<?php the_field('background_video');?>" type="video/mp4">
            </video>
        </figure>

        <div class="text">
            <div class="container">
                <?php $hero_text_thumb = get_field('hero_text');
                $image_size = 'hero_text_thumb';
                $image_url = $hero_text_thumb['sizes'][$image_size];?>
                <img src="<?php echo $image_url; ?>" alt="">
            </div>
        </div>
		<?php if(get_field('show_sound_icon')){?>
        	<i class="play-sound"></i>
		<?php };?>
    </section>
    <?php wp_reset_query();?>
    <div class="info-boxes-list home-page">
        <div class="container">
            <div class="row">  
                          
                <?php echo do_shortcode('[custom-facebook-feed]');?>

            </div>
        </div>    
    </div>        
    <div class="sticky-block">
        <div class="content">
            <?php $args = array(
                'post_type' => 'tribe_events',
                'group_by' => 'date',
                'order' => 'ASC',
                'featured'       => true
            ); 
            $query = new WP_Query($args);
            ?>

            <?php if($query -> have_posts()) { ?>
                <?php while ( $query->have_posts()) { $query->the_post(); ?>
                    <div class="home-event">
                        <div class="event-date d-flex align-items-center justify-content-center">
                            <div>
                                <?php $this_event_meta       = get_post_meta( get_the_ID() );
                                $this_event_start_date = $this_event_meta['_EventStartDate'][0];
                                $start_date_day_number   = date_i18n( 'j.', strtotime( $this_event_start_date ) );
                                $start_date_month_name   = date_i18n( 'F', strtotime( $this_event_start_date ) );
                                $start_date_day_name   = date_i18n( 'l', strtotime( $this_event_start_date ) );
                                ?>
                                <span class="day"><?php echo $start_date_day_number;?></span>
                                <span class="month" style="text-transform:uppercase"><?php echo $start_date_month_name;?></span>
                                <span class="week-day"><?php echo $start_date_day_name;?></span>
                            </div>
                        </div>

                        <h2 class="name">
                            <a href="<?php the_permalink();?>">
                                <?php $organizer_ids = tribe_get_organizer_ids($post);

                                do_action( 'tribe_events_single_meta_organizer_section_start' );

                                foreach ( $organizer_ids as $organizer ) {
                                    if ( ! $organizer ) {
                                        continue;
                                    }

                                    ?>
                                    
                                        <?php echo tribe_get_organizer_link( $organizer ) ?>
                                    <?php
                                }?>
                                <?php the_title();?></a>
                        </h2>
                    </div>
                <?php }
            };?>
        </div>
    </div>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$('.cff-wrapper .col-lg-3.col-md-3.col-sm-6').each(function(){
	if( $(this).find('.thumbnail').html().length == 0 ){
		var href = $(this).find('.cff-viewpost-facebook').attr('href');
		$(this).find('.thumbnail').html('<a href="'+href+'" target="_blank"><img src="<?php the_field('default_facebook_image');?>"></a>');
	}
})
</script>
<?php get_footer(); ?>


<?php 
/*
Template Name: Kontakti
*/ 
get_header(); ?>
<main id="main-content"> 
    <div class="breadcrumb-wrapper">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"><?php the_title();?></li>
            </ol>
        </div>
    </div>
    <?php if(have_posts()) : ?> 
        <?php while ( have_posts()) : the_post(); ?>
            <div class="container">
                <div class="contact-persons">
                    <div class="row">
                        <?php $contacts_group = get_field('contact_info_block');
                        if($contacts_group){
                            foreach($contacts_group as $contacts_block){?>
                                <div class="col-lg-3">
                                    <h2 class="name"><?php echo $contacts_block['name_contacts'];?></h2>
                                    <?php $contacts = $contacts_block['contacts_list'];
                                    if($contacts){?>
                                        <ul>                                           
                                            <?php foreach($contacts as $contact){?>
                                                <li <?php if (filter_var($contact['contact'], FILTER_VALIDATE_EMAIL)) { echo 'class="email"';};?>>
                                                    <?php
                                                    if (filter_var($contact['contact'], FILTER_VALIDATE_EMAIL)) {
                                                        echo '<a href="mailto:'.$contact['contact'].'">'.$contact['contact'].'</a>';
                                                    }else{
                                                        echo $contact['contact'];
                                                    }?>
                                                </li>
                                            <?php };?>
                                        </ul>                                           
                                    <?php };?>
                                </div>
                            <?php };
                        };?>
                    </div>
                </div>
                <?php require get_template_directory() . '/views/location-map.php';?>
            </div>
        <?php endwhile;
    endif;?>
    <div class="sticky-block">
        <div class="content">
            <h3 class="c-name"><span><?php echo _e('TUVĀKIE NOTIKUMI','vef');?></span></h3>
            <?php tribe_get_template_part( '/list' ); ?>
        </div>
    </div>
</main>
<?php get_footer();?>

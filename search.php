<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Shape
 * @since Shape 1.0
 */

get_header(); ?>

    <div class="container search-page">

        <h1 class="page-title"><?php printf( __( ''._e("Meklēšanas rezultāti pēc:", 'vef').' %s', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?></h1>

        <main id="main-content">
            <div class="posts-list">
            <?php get_search_form(); ?>

            <?php 
            $args = array('post_type'=>array('premises','tribe_events'),'s' => get_search_query());
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) : ?>


                <?php /* Start the Loop */ ?>
                <?php while ($the_query-> have_posts() ) : $the_query->the_post(); ?>

                     <article class="post d-flex justify-content-between">
                            <div class="details">
                                <h3 class="entry-title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h3>

                                <p class="excerpt"><?php echo wp_strip_all_tags(excerpt(126)) ?></p>    

                                <time class="entry-time"><?php echo the_time('d.m.Y'); ?></time>
                            </div>
                        </article>

                <?php endwhile; ?>


            <?php else : ?>

                <?php echo _e("Jūsu meklētais netika atrasts! Mēģiniet vēlreiz!", 'cetraszoles'); ?>
            <?php endif; ?>

            </div><!-- #content .site-content -->
        </main><!-- #primary .content-area -->
	</div>


<?php get_footer();
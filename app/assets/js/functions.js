$(function(){

	$('.home .col-lg-3.col-md-3.col-sm-6').each(function() {
  $(this).find('.cff-text').text($(this).find('.cff-text').text().substr(0, 96) + '...');
});
	$('.modal').mouseover(function(){
		var option = $('.modal-body form .filter-option.pull-left').text();
		$('nf-fields-wrap nf-field .hidden .nf-field-element textarea').text(option);
	})
	$(".animated-short").prop('muted', true)
	$(".play-sound").click( function (){
    if( $(".animated-short").prop('muted')){
        $(".animated-short").prop('muted', false);
    } else {
    $(".animated-short").prop('muted', true);
    }

});
	$('.play-sound').click(function(){
		if($('.animated-short').attr('muted')){
			$('.animated-short["muted"]').attr('muted','unmuted');
		}
	})
	$('.archive .holder .btn').click(function(){
		$('nf-field .hidden').removeClass('hidden');
		$('nf-field:nth-child(2)').addClass('hidden');
		$('.modal-body > form').insertBefore('nf-fields-wrap nf-field:nth-child(1)');
	})
	$('.contact-list .telpas-biznesam').click(function(){

		$('nf-field:nth-child(2)').addClass('hidden');
		$('nf-field .hidden').removeClass('hidden');
	})
	$('.contact-list .btn.btn-primary ').click(function(){
		$('.modal-body > form').insertBefore('nf-fields-wrap nf-field:nth-child(1)');
	})
	$('.lang-bar .dropdown-menu li a').addClass('dropdown-item d-flex align-items-center');
	$('.lang-bar .dropdown-menu li a').unwrap();

	$('[data-toggle="tooltip"]').tooltip();
	

	setTimeout(function(){
		$('.home .sticky-block').addClass('show');
	}, 3000);


	if($('.sticky-block .events-list').length) {
		$('.sticky-block .events-list').closest('.sticky-block').addClass('has-events');
	}


	if($('.sticky-block .accordion').length) {
		$('.sticky-block .accordion').closest('.sticky-block').addClass('has-addresses');
	}


	if($('.events-carousel').length) {
		$('.events-carousel').owlCarousel({
			margin: 15,
			items: 4,
			autoHeight:true,
			loop: false,
    		nav: true,
    		dots: false,
    		responsive:{
    			0:{
		            items:1
		        },

		        401:{
		            items:2
		        },

		        768:{
		            items:3
		        },

		        992:{
		        	items:4
		        }
		    }
		});
	}

	/* Place gallery */

	if($('.place-preview .gallery').length) {
		$('.place-preview .gallery .owl-carousel').owlCarousel({
			margin: 0,
			items: 1,
			autoHeight:true,
			loop: false,
    		nav: true,
    		dots: false
		});
	}			

	setTimeout(function(){
		$('.place-preview .gallery .gallery-details .title').text($('.place-preview .gallery .owl-carousel .owl-item.active img').attr('data-title'));
		$('.place-preview .gallery .total-holder .total').text($('.place-preview .gallery .owl-item').length);			
	}, 50);


	$('.place-preview .gallery').on('changed.owl.carousel', function(event) {
		setTimeout(function(){
			$('.place-preview .gallery .gallery-details .title').text($('.place-preview .gallery .owl-carousel .owl-item.active img').attr('data-title'));
			$('.place-preview .gallery .total-holder .current').text($('.place-preview .gallery .owl-carousel .owl-item.active').index() + 1);
		}, 70);
	});

	/* /Place gallery */

	/* RWD Menu */

 	$('#main-header .menu-toggle').on('click', function() {
 		$(this).toggleClass('open');
 		
 		if($(this).hasClass('open')) {
 			if($('#main-header .nav-wrap .sticky-block').length == 0) {
 				$('#main-header .nav-wrap').prepend($('#main-header .second-nav').prop('outerHTML'));
 				$('#main-header .nav-wrap').append($('.sticky-block').prop('outerHTML'));
 			}
 			
			$('#nav-overlay, #main-header .close-menu').addClass('active');
 			$('#main-header .nav-wrap').animate({ right: 0 }, 250);

		} else {
 			$('#main-header .nav-wrap').animate({ right:'-100%' }, 150);
		}
 				
 		return false;
 	});

 	$(document).on('click', '#main-header .close-menu, #nav-overlay, .inhabitant a', function(){
 		$('#main-header .menu-toggle').removeClass('open');

		$('#nav-overlay, #main-header .close-menu').removeClass('active');
		$('#main-header .nav-wrap').animate({right:'-100%' }, 150);	
		
		return false;
	});
	 	$(document).on('click','.inhabitant a', function(){

$('html, body').animate({
			scrollTop: $(".location-map svg").offset().top
		}, 2000);
		})
});
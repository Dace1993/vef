<?php get_header(); ?>

        <main id="main-content"> 
            <div class="breadcrumb-wrapper">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>"><?php echo get_the_title( get_option('page_on_front') );?></a></li>
                        <li class="breadcrumb-item  active" aria-current="page"><?php single_cat_title();?></li>
                    </ol>
                </div>
            </div>

            <div class="container">
                <div class="info-boxes-list">
                    <div class="row">
                        <?php $terms_business = $wpdb->get_var( "SELECT description FROM wp_term_taxonomy WHERE description LIKE '%;i:2;%'") ;
                        function get_string_between($string, $start, $end){
                            $string = ' ' . $string;
                            $ini = strpos($string, $start);
                            if ($ini == 0) return '';
                            $ini += strlen($start);
                            $len = strpos($string, $end, $ini) - $ini;
                            return substr($string, $ini, $len);
                        }
                        $parsed = get_string_between($terms_business, pll_current_language().'";i:', ';');
                        $term = get_queried_object();
                        $download_pdf_banner = get_field('download_pdf_banner','options');
                        if($download_pdf_banner && $term->term_id == $parsed){?>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="info-box download-pdf-banner">
                                    <div class="thumbnail">
                                        <a href="#">
                                            <?php $business_thumb = $download_pdf_banner['image'];
                                            $image_size = 'business_thumb';
                                            $image_url = $business_thumb['sizes'][ $image_size];?>
                                            <img src="<?php echo $image_url;?>">
                                        </a>
                                    </div>

                                    <div class="holder">
                                        <div class="pdf d-flex flex-wrap flex-row justify-content-center align-items-center">
                                            <a href="<?php echo $download_pdf_banner['pdf_banner'];?>" class="info"><?php echo _e('Lejupielādēt','vef');?> <span><?php echo _e('VAIRĀK info','vef');?></span></a>
                                        </div>

                                        <div class="btn-holder d-flex justify-content-end align-items-start flex-wrap flex-column">
                                            <a href="<?php echo $download_pdf_banner['pdf_banner'];?>" class="btn btn-primary" data-toggle="modal" data-target="#request-form"><?php echo _e('NOSŪTIET PIEPRASĪJUMU','vef');?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php };?>
                        <?php wp_reset_query();
                         $cat = get_term_by('name', single_cat_title('',false), 'vieta');
                            $args = array(
                                'post_type' => 'telpas',
                                'group_by' => 'date',
                                'orderby' => 'meta_value_num', 
                                'meta_key' => 'custom_order_type_snv_1', 
                                'order' => 'ASC', 
                                'post_parent'=> 0,
                                'lang' => pll_current_language(),
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'vieta',
                                        'field'    => 'slug',
                                        'terms'    => $cat->slug,
                                    ),
                                ),
                            ); 
                            $query = new WP_Query($args);
                        ?>

                        <?php if($query -> have_posts()) : ?> 
                            <?php while ( $query->have_posts()) : $query->the_post();?>

                                <div class="col-lg-3 col-md-3 col-sm-6" id="<?php echo get_field('house_number');?>" <?php if(!empty(get_field('people_amount'))||empty(get_field('unavailability_text'))){ ?> onClick="window.open('<?php the_permalink();?>','_top' ); return false;" <?php };?>>
                                    <div class="info-box <?php if(get_field('people_amount')){ echo 'has-capacity-hover';}$unavailability_text = get_field('unavailability_text'); if(!empty(get_field('unavailability_text'))&&!empty($unavailability_text['title'])){echo 'has-capacity-hover';};?>">
                                        <div class="thumbnail">
                                            <a href="<?php the_permalink();?>">
                                                <?php the_post_thumbnail('business_thumb');?>
                                            </a>
                                        </div>
                                        <div class="details" <?php $unavailability_text = get_field('unavailability_text'); if(get_field('unavailability_text') &&!empty($unavailability_text['title'])){?> style="opacity:0;" <?php };?>>
                                            <?php $reviews_box = get_field('reviews_box');
                                            if($reviews_box && $term->term_id == $parsed){
                                                $x = 0;
                                                foreach($reviews_box as $review){
                                                    $x++;
                                                    if($x==1){?>
                                                        <blockquote>
                                                            <?php if($review['review_blockquote']){?>
                                                                <cite><?php echo wp_trim_words($review['review_blockquote'], $num_words = 14, $more = '...');?></cite>
                                                            <?php };?>
                                                            <footer>
                                                                <span class="name"><?php echo $review['reviewer_name'];?></span>
                                                            </footer>
                                                        </blockquote>
                                                    <?php };
                                                };
                                            }else{?>
                                                <h2 class="name">
                                                    <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                                </h2>
                                                <span><?php the_field('street_name');?></span>                    
                                            <?php };?>                                            
                                            <a href="<?php the_permalink();?>" class="btn btn-primary"><?php echo _e('UZZINIET VAIRĀK','vef');?></a>
                                        </div>

                                            
                                        <?php if(get_field('people_amount')){?>
                                                <div class="hover-details d-flex justify-content-center align-items-center  flex-column">
                                                    <div class="holder">
                                                        <h2 class="name">
                                                            <?php echo _e('IETILPĪBA','vef');?> <span>/ <?php echo _e('cilvēku skaits','vef');?> /</span>
                                                        </h2>

                                                        <div class="amount"><span><?php the_field('people_amount');?></span></div>
                                                    </div>
                                                    <a href="<?php the_permalink();?>" class="btn btn-primary"><?php echo _e('UZZINIET VAIRĀK','vef');?></a>
                                                </div>
                                        <?php }
                                        $unavailability_text = get_field('unavailability_text');
                                        if(!empty(get_field('unavailability_text'))&&!empty($unavailability_text['title'])){
                                            ?>
                                            <div class="hover-details d-flex justify-content-center align-items-center  flex-column reconstruction" style="opacity:1;">

                                                <div class="holder">
                                                    <h2 class="name">
                                                        <?php echo $unavailability_text['title'];?>
                                                    </h2>

                                                    <div class="message"><p><?php echo $unavailability_text['text'];?></p></div>
                                                </div>

                                            </div>
                                        <?php };?>
                                    </div>
                                </div>
                            <?php endwhile;
                        endif;
                        wp_reset_query();?>
                    </div>
                </div>

                <div class="modal fade" id="request-form" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header d-flex justify-content-end">
                                <h4 class="modal-title"><span><i><?php echo _e('NOSŪTIET PIEPRASĪJUMU','vef');?></i></span></h4>
                            </div>

                            <div class="modal-body">
                                <?php echo do_shortcode('[ninja_form id=1]');?>
                            </div>
                        </div>
                    </div>
                 </div>

                <?php require get_template_directory() . '/views/location-map.php';?>
            </div>
            <?php
            $inhabitants_block = get_field('inhabitants_block','options');
            if($inhabitants_block && $term->term_id == $parsed){?>
                <div class="sticky-block">
                    <div class="content">
                        <h3 class="c-name"><span><?php echo _e('VEF KVARTĀLA IEMĪTNIEKI','vef');?></span></h3>
                        <?php $inhabitants_list = $inhabitants_block['inhabitants_list'];
                        if($inhabitants_list){?>
                            <div id="accordion" class="accordion" role="tablist">
                                <?php $count = 0;
                                foreach($inhabitants_list as $list){
                                    $count++;?>
                                    <div class="card">
                                        <div class="card-header" role="tab" id="heading-<?php echo $count;?>">
                                            <h5>
                                                <a data-toggle="collapse" href="#collapse-<?php echo $count;?>" role="button" aria-expanded="false" aria-controls="collapse-<?php echo $count;?>"><span><?php echo $list['street_name'];?></span></a>
                                            </h5>
                                        </div>
                                        <?php $streets = $list['streets'];
                                        if($streets){?>
                                            <div id="collapse-<?php echo $count;?>" class="collapse" role="tabpanel" aria-labelledby="heading-<?php echo $count;?>" data-parent="#accordion">
                                                <div class="card-body">
                                                    <ul class="list">
                                                        <?php foreach($streets as $street){
                                                            if($street['inhabitant']){?>
                                                                <li class="inhabitant"><a class="<?php echo $street['house'];?>"><?php echo $street['inhabitant'];?></a></li>
                                                            <?php };
                                                        };?>
                                                    </ul>
                                                </div>
                                            </div>
                                        <?php };?>
                                    </div>
                                <?php };?>
                            </div>
                        <?php };?>
                    </div>
                </div>
            <?php }elseif($inhabitants_block && $term->term_id !== $parsed && get_field('events_show','options')){?>
                <div class="sticky-block has-events">
                    <div class="content">
                        <h3 class="c-name"><span><?php echo _e('TUVĀKIE NOTIKUMI','vef');?></span></h3>
                        <?php tribe_get_template_part( '/list' ); ?>
                    </div>
                </div>
            <?php };?>
        </main>

    <?php
get_footer(); ?>

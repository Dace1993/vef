<?php

/**
 * SW engine. 
 * Global Template functions.
 */

require get_template_directory() . '/inc/base.php';

/**
 * Structure.
 * Template functions used throughout the theme.
 */

require get_template_directory() . '/inc/theme.php';

/**
 * Widgets.
 * Template widgets used throughout the theme.
 */

require get_template_directory() . '/inc/widgets.php';


function simple_pagination($pages = '', $range = 2)
{  
   $showitems = ($range * 2)+1;  

   global $paged;
    if(empty($paged)) $paged = 1;

   if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }  

   if(1 != $pages)
    {?>

<nav aria-label="Page navigation">

<ul class="pagination justify-content-center align-items-center flex-wrap">
    <?php if($paged > 1 ){
           echo '<li class="page-item"> <a class="page-link prev d-flex align-items-center justify-content-center" href="'.get_pagenum_link($paged - 1).'"><i class="fas fa-caret-left"></i></a></li>';
        }
         else {
             echo '<li class="page-item disabled prev"> <a class="page-link" href="'.get_pagenum_link(1).'"><i class="fas fa-caret-left"></i></a></li>';
            }
    for ($i=1; $i <= $pages; $i++){
        if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
            echo ($paged == $i)? '<li class="page-item "><a class="page-link active" href="'.get_pagenum_link($i).'">'.$i.'</a></li>':'<li class="page-item"><a class="page-link" href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
        }
    }
    if ($paged < $pages){
           //echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";
           echo '<li class="page-item"><a class="page-link next d-flex align-items-center justify-content-center" href="'.get_pagenum_link($paged + 1).'"><i class="fas fa-caret-right"></i></a></li>';
        }
        else{
          echo '<li class="page-item disabled next"><a class="page-link " href="'.get_pagenum_link($paged + 1).'"><i class="fas fa-caret-right"></i></a></li>';
        }?>

</ul>
       <
   <?php     
 
    }
}




add_action('registered_post_type', 'igy2411_make_posts_hierarchical', 10, 2 );

// Runs after each post type is registered
function igy2411_make_posts_hierarchical($post_type, $pto){

    // Return, if not post type posts
    if ($post_type != 'premises') return;

    // access $wp_post_types global variable
    global $wp_post_types;

    // Set post type "post" to be hierarchical
    $wp_post_types['premises']->hierarchical = 1;

    // Add page attributes to post backend
    // This adds the box to set up parent and menu order on edit posts.
    add_post_type_support( 'premises', 'page-attributes' );

}


add_theme_support( 'post-formats', 'premises');

add_filter( 'pre_get_posts', 'add_cpt_in_search_result' );

function add_cpt_in_search_result( $query ) {

    if ( $query->is_search ) {
    $query->set( 'post_type', array( 'post', 'page', 'premises','tribe_events' ) );
    }

    return $query;
}


//add SVG to allowed file uploads
function add_file_types_to_uploads($file_types){

    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );

    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');
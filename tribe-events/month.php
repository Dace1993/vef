<?php
/**
 * Month View Template
 * The wrapper template for month view.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
?>
<?php get_header(); ?>
    <main id="main-content"> 
        <div class="breadcrumb-wrapper">
            <div class="container">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>"><?php echo get_the_title( get_option('page_on_front') );?></a></li>
                <li class="breadcrumb-item  active" aria-current="page"><?php echo _e('IZKLAIDE','vef');?></li>
                </ol>
            </div>
        </div>

        <div class="container">
            <div class="events-categories-links">
                <ul class="d-sm-flex">
                    <li <?php if( tribe_is_month() && !is_tax()){echo 'class="active"';};?>><a href="<?php echo get_home_url(); ?>/events"><?php echo _e('Visi','vef');?></a></li>
                    <?php $cat = get_term_by('name', single_cat_title('',false), 'tribe_events_cat');?>

                    <?php $term_list = get_terms(array('taxonomy' => 'tribe_events_cat','hide_empty' => true));
                        foreach($term_list as $term_single) {?>
                        	<li <?php if($cat){if($term_single->slug == $cat->slug){echo 'class="active"';};}?>><a href="<?php echo get_term_link($term_single); ?>"><?php echo $term_single->name; ?></a></li>
                    <?php }?>
                </ul>
            </div>

            <div class="events-carousel owl-carousel">
                <?php wp_reset_query();
                if($cat){             
                    $args = array(
                        'post_type' => 'tribe_events',
                        'group_by' => 'date',
                        'order' => 'ASC',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'tribe_events_cat',
                                'field'    => 'slug',
                                'terms'    => $cat->slug,
                            ),
                        ),
                        'meta_query'    => array(
                            array(
                                'key'       => '_EventEndDate',
                                'value'     => current_time( 'mysql' ),
                                'compare'      => '>',
                            ),
                        ),
                    ); 
                }else{
                	$args = array(
                        'post_type' => 'tribe_events',
                        'group_by' => 'date',
                        'order' => 'ASC',
                        'meta_query'    => array(
                            array(
                                'key'       => '_EventEndDate',
                                'value'     => current_time( 'mysql' ),
                                'compare'      => '>',
                            ),
                        ),
                    );
                }
                    $query = new WP_Query($args);
                ?>

                <?php if($query -> have_posts()) { ?>
                <?php while ( $query->have_posts()) : $query->the_post(); ?>
                    <div>
                        <div class="info-box">
                            <div class="thumbnail">
                                <a href="<?php the_permalink();?>">
                                    <?php the_post_thumbnail('business_thumb');?>
                                </a>
                            </div>

                            <div class="details">
                                <h2 class="name">
                                	<a href="<?php the_permalink();?>">
	                                	<?php $organizer_ids = tribe_get_organizer_ids();

										do_action( 'tribe_events_single_meta_organizer_section_start' );

										foreach ( $organizer_ids as $organizer ) {
											if ( ! $organizer ) {
												continue;
											}

											?>
											
												<?php echo tribe_get_organizer_link( $organizer ) ?>
											<?php
										}?>
									</a>
                                </h2>

                                <span><?php the_title();?></span>

                                <a href="<?php the_permalink();?>" class="btn btn-primary"><?php echo _e('UZZINIET VAIRĀK','vef');?></a>
                            </div>

                            <div class="event-date d-flex align-items-center justify-content-center">
                                <div>
                                	<?php $this_event_meta       = get_post_meta( get_the_ID() );
									$this_event_start_date = $this_event_meta['_EventStartDate'][0];
									$start_date_day_number   = date_i18n( 'j.', strtotime( $this_event_start_date ) );
									$start_date_month_name   = date_i18n( 'F', strtotime( $this_event_start_date ) );
									$start_date_day_name   = date_i18n( 'l', strtotime( $this_event_start_date ) );
									?>
                                    <span class="day"><?php echo $start_date_day_number;?></span>
                                    <span class="month" style="text-transform:uppercase"><?php echo $start_date_month_name;?></span>
                                    <span class="week-day"><?php echo $start_date_day_name;?></span>
                                </div>
                            </div>
                        </div>
                    </div>        
    			<?php endwhile; }?>
            </div>
            <?php require get_template_directory() . '/views/location-map.php';?>
        </div>

        <div class="sticky-block">
            <div class="content">
                <h3 class="c-name"><span><?php echo _e('TUVĀKIE NOTIKUMI','vef');?></span></h3>

                <?php tribe_get_template_part( '/list' ); ?>

            </div>
        </div>
    </main>
<?php get_footer(); ?>



<?php
/**
 * Month View Content Template
 * The content template for the month view of events. This template is also used for
 * the response that is returned on month view ajax requests.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/month/content.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
} ?>

<div id="tribe-events-content" class="tribe-events-month">

	<!-- Notices -->
	<?php tribe_the_notices() ?>


		<!-- Header Navigation -->
		<?php tribe_get_template_part( 'month/nav' ); ?>



	<!-- Month Grid -->
 <ul id="past_events">
<?php $args = array( 'post_type' => 'tribe_events', 'posts_per_page' => 5, 'eventDisplay' => 'past' );
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post(); ?>
<li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
<?php endwhile; ?>
</ul>
</div><!-- .tribe-events-loop -->
	<!-- Month Footer -->
	<?php do_action( 'tribe_events_before_footer' ) ?>

		<?php tribe_get_template_part( 'month/nav' ); ?>


	<!-- #tribe-events-footer -->
	<?php do_action( 'tribe_events_after_footer' ) ?>

	<?php tribe_get_template_part( 'month/mobile' ); ?>
	<?php tribe_get_template_part( 'month/tooltip' ); ?>

</div><!-- #tribe-events-content -->

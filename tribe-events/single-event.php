<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>

<main id="main-content"> 
    <div class="breadcrumb-wrapper">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>"><?php echo get_the_title( get_option('page_on_front') );?></a></li>
                <li class="breadcrumb-item"><a href="<?php echo esc_url( tribe_get_events_link() ); ?>"><?php echo _e('IZKLAIDE','vef');?></a></li>
            </ol>
        </div>
    </div>
	<?php tribe_the_notices() ?>
	<div class="container">
		<div class="place-preview">
            <div class="row">
                <div class="col-lg-6">
                	<?php $gallery = get_field('event_gallery');
                    if($gallery || the_post_thumbnail()){?>
						<div class="gallery">
	                        <div class="owl-carousel">
	                    
	                        	<?php foreach($gallery as $gallery_image){?>
									<div>

                                        <a data-fancybox="gallery" href="<?php echo $gallery_image['image']['url'];?>">
                                            <?php $gallery_thumb = $gallery_image['image'];
                                            $image_size = 'gallery_thumb';
                                            $image_url = $gallery_thumb['sizes'][$image_size];?>
                                            <img src="<?php echo $image_url;?>" alt="" data-title="<?php echo $gallery_image['image']['title'];?>">
                                        </a>
                                    </div>
                                <?php };?>
	                    	</div>
	                    	<div class="gallery-details d-flex justify-content-between">                       	
	                        	<h5 class="title"></h5> 
	                        	<div class="total-holder"><i class="current">1</i><i class="total"></i></div>
	                        </div>
	                    </div>
	                <?php };?>
	                <div class="text">
		                <?php the_content();?>
		            </div>
                </div>
				<div class="col-lg-6">
                    <div class="summary">
                        <h1 class="name"><?php the_title(); ?></h1>
		                <div class="details">
		                    <table class="table">
		                        <tbody>
									<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
									<?php tribe_get_template_part( 'modules/meta' ); ?>
									<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
		    </div>
		</div>
		<?php require get_template_directory() . '/views/location-map.php';?>
    </div>
    <div class="sticky-block">
        <div class="content">
            <h3 class="c-name"><span><?php echo _e('TUVĀKIE NOTIKUMI','vef');?></span></h3>
            <?php tribe_get_template_part( '/list' ); ?>
        </div>
    </div>
</main>
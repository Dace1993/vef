<div class="events-list">
	<ul class="flex-wrap">
	   <?php wp_reset_query();
	   $events = tribe_get_events(array('meta_query'    => array(
            array(
	                'key'       => '_EventEndDate',
	                'value'     => current_time( 'mysql' ),
	                'compare'      => '>',
	            ),
	        )
        ));
	   $count=0;
		foreach ( $events as $post ) {
			$count++;
		    setup_postdata( $post );?>
		    <?php if($count<5){?>
		        <li class="event d-flex align-items-start">
	                <div class="event-date d-flex align-items-center justify-content-center">
	                    <div>
	                    	<?php $this_event_meta       = get_post_meta( $post->ID );
							$this_event_start_date = $this_event_meta['_EventStartDate'][0];
							$start_date_day_number   = date_i18n( 'j.', strtotime( $this_event_start_date ) );
							$start_date_month_name   = date_i18n( 'F', strtotime( $this_event_start_date ) );
							$start_date_day_name   = date_i18n( 'l', strtotime( $this_event_start_date ) );
							?>
	                        <span class="day"><?php echo $start_date_day_number;?></span>
	                        <span class="month" style="text-transform:uppercase"><?php echo $start_date_month_name;?></span>
	                        <span class="week-day"><?php echo $start_date_day_name;?></span>
	                    </div>
	                </div>	            

	                <div class="details">
	                    <h2 class="name">
	                    	<a href="<?php echo get_the_permalink($post);?>">
	                        	<?php $organizer_ids = tribe_get_organizer_ids($post);

								do_action( 'tribe_events_single_meta_organizer_section_start' );

								foreach ( $organizer_ids as $organizer ) {
									if ( ! $organizer ) {
										continue;
									}

									?>
									
										<?php echo tribe_get_organizer_link( $organizer ) ?>
									<?php
								}?>
							</a>
	                    </h2>

	                    <span><?php echo get_the_title($post);?></span>

	                    <a href="<?php echo get_the_permalink($post);?>" class="btn btn-primary"><?php echo _e('UZZINIET VAIRĀK','vef');?></a>
	                </div>
		        </li>        
			<?php };
		}
	?>
	</ul>
</div>
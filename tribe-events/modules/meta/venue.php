<?php
/**
 * Single Event Meta (Venue) Template
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe-events/modules/meta/venue.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 */

if ( ! tribe_get_venue_id() ) {
	return;
}

$phone   = tribe_get_phone();
$website = tribe_get_venue_website_link();

?>

	<td class="title"> <?php esc_html_e( tribe_get_venue_label_singular(), 'the-events-calendar' ) ?>: </td>
	<td>
		<div class="contact-list">
			<ul>
				<?php do_action( 'tribe_events_single_meta_venue_section_start' ) ?>

				<li class="tribe-venue"> <?php echo tribe_get_venue() ?> </li>

				<?php if ( tribe_address_exists() ) : ?>
					<li class="tribe-venue-location">
						<address class="tribe-events-address">
							<?php echo tribe_get_full_address(); ?>

							<?php if ( tribe_show_google_map_link() ) : ?>
								<?php echo tribe_get_map_link_html(); ?>
							<?php endif; ?>
						</address>
					</li>
				<?php endif; ?>

				<?php if ( ! empty( $phone ) ): ?>
					<li><?php esc_html_e( 'Phone:', 'the-events-calendar' ) ?>
						<?php echo $phone ?> 
					</li>
				<?php endif ?>

				<?php if ( ! empty( $website ) ): ?>
					<dt class="tribe-venue-url-label"> <?php esc_html_e( 'Website:', 'the-events-calendar' ) ?> </dt>
					<dd class="tribe-venue-url"> <?php echo $website ?> </dd>
				<?php endif ?>

				<?php do_action( 'tribe_events_single_meta_venue_section_end' ) ?>
			</ul>
		</div>
	</td>

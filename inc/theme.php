<?php

/**
 * vef styles. 
 * vef css/js assets.
 */

add_action('init', 'my_register_styles');

function my_register_styles() {
    
}


function vef_enqueue_styles() {
  
}

add_action('wp_enqueue_scripts', 'vef_enqueue_styles');

function vef_adding_scripts() {
  
}
add_action( 'wp_enqueue_scripts', 'vef_adding_scripts' ); 



add_theme_support( 'post-thumbnails' );

add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );

function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}
/**
 * vef menus. 
 * vef side menu.
 */

function register_my_menus() {
  register_nav_menus(
    array(  
      'top_navigation' => __( 'Top menu '),
    )
  );
} 
add_action( 'init', 'register_my_menus' );

require_once('wp_bootstrap_navwalker.php');

add_image_size( 'business_thumb', 245, 332, true );
add_image_size( 'gallery_thumb', 1404, 682, true );
add_image_size( 'building_capacity_thumb', 300, 73, true );
add_image_size( 'hero_text_thumb', 300, 139, true);



function post_image_sizes($sizes){
  $custom_sizes = array(
    'business_thumb' => 'Business thumbnail',
    'gallery_thumb' => 'Gallery thumbnail',
    'building_capacity_thumb' => 'Building capacity thumbnail',
    'hero_text_thumb' => 'Hero text thumbnail',
  );
  return array_merge( $sizes, $custom_sizes );
}
add_filter('image_size_names_choose', 'post_image_sizes');

if ( ! current_user_can( 'manage_options' ) ) {
    show_admin_bar( false );
}

function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}
 
function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  } 
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}

add_filter( 'wp_nav_menu_objects', 'add_has_children_to_nav_items' );

function add_has_children_to_nav_items( $items )
{
    $parents = wp_list_pluck( $items, 'menu_item_parent');

    foreach ( $items as $item )
        in_array( $item->ID, $parents ) && $item->classes[] = 'has-children';

    return $items;
}

function wpse_remove_empty_links( $menu ) {
    return str_replace( '<a href="#">', '', $menu );
}

add_filter( 'wp_nav_menu_items', 'wpse_remove_empty_links' );

class WPSE_78121_Sublevel_Walker extends Walker_Nav_Menu
{
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class='sub-menu'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }
}

if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page();
  
}

add_filter( 'post_thumbnail_html', 'remove_size_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_size_attribute', 10 );
function remove_size_attribute( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

add_action( 'vef_attributes', 'cj_show_attribute_links' );
function cj_show_attribute_links() {
  global $post;
  $attribute_names = array( 'pa_razotajs'); // Insert attribute names here
  foreach ( $attribute_names as $attribute_name ) {
    $taxonomy = get_taxonomy( $attribute_name );
    if ( $taxonomy && ! is_wp_error( $taxonomy ) ) {
      $terms = wp_get_post_terms( $post->ID, $attribute_name );
      $terms_array = array();
          if ( ! empty( $terms ) ) {
            foreach ( $terms as $term ) {
             $archive_link = get_term_link( $term->slug, $attribute_name );
             $full_line = '<a href="' . $archive_link . '">'. $term->name . '</a>';
             array_push( $terms_array, $full_line );
            }
            echo implode( $terms_array, ', ' );
          }
      }
    }
}

add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');



if( defined( 'YITH_WCWL' ) && ! function_exists( 'yith_wcwl_ajax_update_count' ) ){
function yith_wcwl_ajax_update_count(){
wp_send_json( array(
'count' => yith_wcwl_count_all_products()
) );
}
add_action( 'wp_ajax_yith_wcwl_update_wishlist_count', 'yith_wcwl_ajax_update_count' );
add_action( 'wp_ajax_nopriv_yith_wcwl_update_wishlist_count', 'yith_wcwl_ajax_update_count' );
}


add_action( 'after_setup_theme', 'my_theme_setup' );
function my_theme_setup(){
    load_theme_textdomain( 'vef', get_template_directory() . '/languages' );
}
    <?php get_header(); ?>
    <?php wp_reset_query();
    $parents = get_post_ancestors( $post->ID );
    $id = ($parents) ? $parents[count($parents)-1]: $post->ID;?>
    <?php $terms_business = $wpdb->get_var( "SELECT description FROM wp_term_taxonomy WHERE description LIKE '%;i:2;%'") ;
    function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
    $parsed = get_string_between($terms_business, pll_current_language().'";i:', ';');?>
        <main id="main-content"> 
            <div class="breadcrumb-wrapper">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo get_home_url(); ?>"><?php echo get_the_title( get_option('page_on_front') );?></a></li>
                        <?php $term_list = wp_get_post_terms($post->ID, 'vieta', array("fields" => "all"));
                        foreach($term_list as $term_single) {?>
                        <li class="breadcrumb-item"><a href="<?php echo get_term_link($term_single); ?>"><?php echo $term_single->name; ?></a></li>
                        <?php }?>
                        <li class="breadcrumb-item  active" aria-current="page"><?php echo get_the_title($id);?></li>
                    </ol>
                </div>
            </div>
             
            <div class="container">
                <?php               
                    $args = array(
                        'post_type' => 'telpas',
                        'orderby' => 'meta_value_num', 
                        'meta_key' => 'custom_order_type_snv_1',
                        'post_parent'=> $post->ID                         
                    ); 
                    $query = new WP_Query($args);
                ?>

                <?php if($query -> have_posts()) { ?>                 
                    <div class="info-boxes-list people-capacity">
                        <div class="row">                            
                            <?php while ( $query->have_posts()) : $query->the_post(); ?>
                                <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="info-box has-text">
                                        <div class="thumbnail">
                                            <a href="<?php the_permalink();?>">
                                                <?php the_post_thumbnail('business_thumb');?>
                                            </a>
                                        </div>
                                        <div class="details d-flex justify-content-center align-items-center">
                                            <div class="holder">
                                                <h2 class="name">
                                                    <span><?php the_title();?></span> <?php the_field('bottom_title');?>
                                                </h2>

                                                <div class="message">
                                                    <p><?php echo wp_strip_all_tags(excerpt(7)) ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    
                                            <?php if(get_field('people_amount')&&!get_field('unavailability_text')){?>
                                                <div class="hover-details d-flex justify-content-center align-items-center">

                                                <div class="holder">
                                                    <h2 class="name">
                                                        <?php echo _e('IETILPĪBA','vef');?> <span>/ <?php echo _e('cilvēku skaits','vef');?> /</span>
                                                    </h2>

                                                    <div class="amount"><span><?php the_field('people_amount');?></span></div>
                                                </div>
                                                <a href="<?php the_permalink();?>" class="btn btn-primary"><?php echo _e('UZZINIET VAIRĀK','vef');?></a>
                                            </div>
                                            <?php }elseif(!get_field('unavailability_text')&&!get_field('unavailability_text')['text']||!get_field('unavailability_text')['title']){?>
                                            <div class="hover-details d-flex justify-content-center align-items-center">
                                                <a href="<?php the_permalink();?>" class="btn btn-primary"><?php echo _e('UZZINIET VAIRĀK','vef');?></a>
                                            </div>
                                            <?php }
                                            $unavailability_text = get_field('unavailability_text');
                                            if(get_field('unavailability_text')&&$unavailability_text['text']||$unavailability_text['title']){?>
                                                <div class="hover-details d-flex justify-content-center align-items-center reconstruction">
                                                    <div class="holder">
                                                        <h2 class="name">
                                                            <?php echo $unavailability_text['title'];?>
                                                        </h2>

                                                        <div class="message"><p><?php echo $unavailability_text['text'];?></p></div>
                                                    </div>
                                                </div>
                                            <?php };?>
                                           
                                        
                                    </div>                            
                                </div>
                            <?php endwhile;
                            wp_reset_query();?>
                        </div>
                    </div>
                <?php }else{?>
                    <?php if(have_posts()) : ?>
                        <?php while ( have_posts()) : the_post(); ?>
                            <div class="place-preview">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <?php $gallery = get_field('gallery');
                                        if($gallery){?>
                                            <div class="gallery">
                                                <div class="owl-carousel">
                                                    <?php foreach($gallery as $gallery_image){?>
                                                        <div>

                                                            <a data-fancybox="gallery" href="<?php echo $gallery_image['image']['url'];?>">
                                                                <?php $gallery_thumb = $gallery_image['image'];
                                                                $image_size = 'gallery_thumb';
                                                                $image_url = $gallery_thumb['sizes'][$image_size];?>
                                                                <img src="<?php echo $image_url;?>" alt="" data-title="<?php echo $gallery_image['image_description'];?>">
                                                            </a>
                                                        </div>
                                                    <?php };?>
                                                    
                                                </div>


                                                <div class="gallery-details d-flex justify-content-between">
                                                    <h5 class="title"></h5> <div class="total-holder"><i class="current">1</i> <i class="total"></i></div>
                                                </div>

                                            </div>
                                        <?php };?>
                                        <div class="text">
                                            <?php the_content();?>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="summary">
                                            <h1 class="name"><?php the_title();?></h1>

                                            <div class="details">
                                                <table class="table">
                                                    <tbody>
                                                        <?php $contacts_group = get_field('contacts_group');
                                                        if($contacts_group){?>
                                                            <tr class="contacts">
                                                                <td class="title"><?php echo _e('Kontakti','vef');?></td>
                                                                <td>
                                                                    <div class="contact-list">
                                                                        <ul>
                                                                            <li class="contact-name"><?php echo $contacts_group['contactpersons_name'];?></li>
                                                                            <?php $contacts = $contacts_group['contacts_list'];
                                                                            if($contacts){
                                                                                foreach($contacts as $contact){?>
                                                                                    <li <?php if (filter_var($contact['contact'], FILTER_VALIDATE_EMAIL)) { echo 'class="email"';};?>>
                                                                                    <?php
                                                                                    if (filter_var($contact['contact'], FILTER_VALIDATE_EMAIL)) {
                                                                                        echo '<a href="mailto:'.$contact['contact'].'">'.$contact['contact'].'</a>';
                                                                                    }else{
                                                                                        echo $contact['contact'];
                                                                                    }?>
                                                                                </li>
                                                                                <?php };
                                                                            };?>
                                                                        </ul>
                                                                        <a href="#" class="btn btn-primary <?php $terms_business = get_terms(array('taxonomy'=> 'vieta', 'lang' => pll_default_language()));
                                                                            foreach($terms_business as $business){ if($term_single->term_id == $parsed ){echo ' telpas-biznesam';};}?>" data-toggle="modal" data-target="#request-form"><?php echo _e('NOSŪTIET PIEPRASĪJUMU','vef');?>
                                                                                
                                                                        </a>

                                                                        <div class="modal fade" id="request-form" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
                                                                            <div class="modal-dialog" role="document">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header d-flex justify-content-end">
                                                                                        <h4 class="modal-title"><span><i><?php echo _e('NOSŪTIET PIEPRASĪJUMU','vef');?></i></span></h4>
                                                                                    </div>

                                                                                    <div class="modal-body">
                                                                                        <form>
                                                                                            
                                                                                        

                                                                                            <?php wp_reset_query();
                                                                        
                                                                                                $args = array(
                                                                                                    'post_type' => 'telpas',
                                                                                                    'group_by' => 'date',
                                                                                                    'order' => 'DESC',
                                                                                                    'post_parent'=> 0,
                                                                                                    'tax_query' => array(
                                                                                                        array(
                                                                                                            'taxonomy' => 'vieta',
                                                                                                            'field'    => 'slug',
                                                                                                            'terms'    => 'telpas-pasakumiem',
                                                                                                        ),
                                                                                                    ),
                                                                                                ); 
                                                                                                $query = new WP_Query($args);
                                                                                            ?>

                                                                                                <?php if($query -> have_posts()){
                                                                                                   if($term_single->term_id == $parsed) { 
                                                                                                        

                                                                                                    }else{$x =0;?> 
                                                                                                    <label><?php echo _e('TELPAS NOSAUKUMS','vef');?></label>
                                                                                                        <select class="selectpicker">
                                                                                                        <?php while ( $query->have_posts()) { $query->the_post(); ?>
                                                                                                            <option value="<?php the_title();?>"><?php the_title();?></option>
                                                                                                        <?php }
                                                                                                    }
                                                                                                };?>
                                                                                                </select>
                                                                                            <?php wp_reset_query();?>
                                                                                        </form>
                                                                                        <?php echo do_shortcode('[ninja_form id=1]');?>


                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php };?>
                                                        <?php $technical_plan = get_field('technical_plan');
                                                        if($technical_plan){?>
                                                            <tr class="technical-plan">
                                                                <td class="title"><?php echo _e('TEHNISKAIS PLĀNS','vef');?></td>
                                                                <td>
                                                                    <a href="<?php echo $technical_plan;?>" class="download-pdf"></a>
                                                                </td>
                                                            </tr>
                                                        <?php };?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php if(get_field('building_capacity')){?>
                                                <div class="building-capacity">
                                                    <?php $building_capacity_thumb = get_field('building_capacity');
                                                    $image_sizes = 'building_capacity_thumb';
                                                    $image_urls = $building_capacity_thumb['sizes'][$image_sizes];?>
                                                    <img src="<?php echo $image_urls;?>">
                                                </div>
                                            <?php };?>
                                        </div>
                                        <?php if(get_field('text_block')){?>
                                            <div class="text">
                                                <?php the_field('text_block');?>
                                            </div>
                                        <?php };?>
                                    </div>
                                    <?php $reviews_box = get_field('reviews_box');
                                    if($reviews_box){?>
                                        <div class="reviews row">
                                            <div class="col-lg-12">
                                                <h4><?php echo _e('Atsauksmes','vef');?></h4>
                                            </div>
                                            <?php foreach($reviews_box as $review){?>
                                                <div class="col-lg-4">
                                                    <blockquote>
                                                        <?php if($review['review_blockquote']){?>
                                                            <cite><?php echo $review['review_blockquote'];?></cite>
                                                        <?php };?>
                                                        <footer>
                                                            <span class="name"><?php echo $review['reviewer_name'];?></span>
                                                        </footer>
                                                    </blockquote>
                                                </div>
                                            <?php };?>
                                        </div>
                                    <?php };?>
                                </div>
                            </div>
                        <?php endwhile;?>
                    <?php endif;?>
                <?php };?>
                

                <?php require get_template_directory() . '/views/location-map.php';?>
            </div>
            
            <?php $inhabitants_block = get_field('inhabitants_block','options');

            if($inhabitants_block && $term_single->term_id == $parsed ){?>
                <div class="sticky-block has-addresses">
                    <div class="content">
                        <h3 class="c-name"><span><?php echo _e('VEF KVARTĀLA IEMĪTNIEKI','vef');?></span></h3>
                        <?php $inhabitants_list = $inhabitants_block['inhabitants_list'];
                        if($inhabitants_list){?>
                            <div id="accordion" class="accordion" role="tablist">
                                <?php $count = 0;
                                foreach($inhabitants_list as $list){
                                    $count++;?>
                                    <div class="card">
                                        <div class="card-header" role="tab" id="heading-<?php echo $count;?>">
                                            <h5>
                                                <a data-toggle="collapse" href="#collapse-<?php echo $count;?>" role="button" aria-expanded="false" aria-controls="collapse-<?php echo $count;?>"><span><?php echo $list['street_name'];?></span></a>
                                            </h5>
                                        </div>
                                        <?php $streets = $list['streets'];
                                        if($streets){?>
                                            <div id="collapse-<?php echo $count;?>" class="collapse" role="tabpanel" aria-labelledby="heading-<?php echo $count;?>" data-parent="#accordion">
                                                <div class="card-body">
                                                    <ul class="list">
                                                        <?php foreach($streets as $street){
                                                            if($street['inhabitant']){?>
                                                                <li class="inhabitant"><a class="<?php echo $street['house'];?>"><?php echo $street['inhabitant'];?></a></li>
                                                            <?php };
                                                        };?>
                                                    </ul>
                                                </div>
                                            </div>
                                        <?php };?>
                                    </div>
                                <?php };?>
                            </div>
                        <?php };?>
                    </div>
                </div>
            <?php }elseif(get_field('events_show','options')){?>
                <div class="sticky-block has-events">
                    <div class="content">
                        <h3 class="c-name"><span><?php echo _e('TUVĀKIE NOTIKUMI','vef');?></span></h3>
                        <?php tribe_get_template_part( '/list' ); ?>
                    </div>
                </div>
            <?php };?>
        </main>

    
<?php get_footer(); ?>

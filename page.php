
<?php 	if (tribe_is_event() || tribe_is_event_category() || tribe_is_in_main_loop() || tribe_is_view() || 'tribe_events' == get_post_type() || is_singular( 'tribe_events' )) {
	    			get_header(); 

        if(have_posts()) {
    		while ( have_posts()) { the_post();

    			 the_content();

    			}
        }
get_footer(); 
   }else{
  	get_template_part( 'single', 'premises' );
  };?>

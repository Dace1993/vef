<?php 

get_header(); ?>
<main id="main-content">
    <div class="container">
        <h1 class="page-name text-center"><?php echo get_the_title(get_option( 'page_for_posts' ));?></h1>
        
        <div class="posts-list items-list">
            <div class="row">
                <?php wp_reset_query();
                $args = array(
                    'post_type' => 'post',
                    'orderby' => 'date',
                    'post_per_page' => -1,
                );
                $my_query = new WP_Query( $args );

                if($my_query -> have_posts()) : ?> 
                    <?php while (  $my_query -> have_posts()) :  $my_query -> the_post(); ?>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <article class="post">
                                <div class="featured-image">
                                    <a href="<?php the_permalink();?>">
                                        <?php the_post_thumbnail('post_thumb');?>
                                        <i class="btn btn-primary"><?php echo _e('LASĪT VISU','vef');?></i>
                                    </a>
                                </div>

                                <div class="details">
                                    <h2 class="entry-title">
                                        <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                    </h2>

                                    <p class="excerpt"><?php echo wp_strip_all_tags(excerpt(10)) ?></p>
                                </div>
                            </article>
                        </div>
                    <?php endwhile;
                endif;?>                
            </div>
            <div class="load-more text-center">
                <a class="btn btn-primary"><?php echo _e('IELĀDĒT VAIRĀK','vef');?></a>
            </div>
        </div>
    </div>
</main>
<script>
(function ($) {
    $(document).ready(function () {
    size_li = $(".col-lg-4.col-md-6.col-sm-6").size();
    x=6;
     if (size_li <= x){
        $(".load-more").addClass("hidden");
     }
     

    $('.col-lg-4.col-md-6.col-sm-6:lt('+x+')').show();
    $('.load-more').click(function () {
        x=(x+6);

        $('.col-lg-4.col-md-6.col-sm-6:lt('+x+')').show();
        if (size_li<=x){
        $(".load-more").addClass("hidden");
     }
      
    });

 
});
        
})(jQuery);
</script>
<?php get_footer();?>
<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">	
	<div class="d-flex align-items-center">    
        <input type="text" class="form-control" value="<?php echo get_search_query(); ?>" name="s" id="s" />
        <button type="submit" id="searchsubmit" value="<?php echo esc_attr_x( '', 'submit button' ); ?>"></button>	 
    </div>
</form>
